<?php 
/**
 * Template Name: homepage
 * Default display index page
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @package christileeson.com Theme
 * @author Christi Leeson 
 */
get_header();
?>
	<div class="header-outer">
		<div class="header-inner">
			<header class="top-header">
				<h1 class="site-title">
					<a href="<?php echo esc_url( home_url() ); ?>">
						<img class="dark" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-christi-leeson-dk-bg.png" 
							alt="Christi Leeson">
						<img class="light" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-christi-leeson.png" 
							alt="Christi Leeson">
					</a>
				</h1>
				<?php 
					wp_nav_menu( array(
						'theme_location'	=> 'header-nav',
						'container'			=> 'nav'
					));
				?>
				<button class="glyphicon glyphicon-menu-hamburger" data-toggle="collapse" data-target="#menu-side"></button>
			</header>
		</div>
	</div>
	<div class="banner-outer">
		<div class="banner-inner">
			<?php get_template_part( 'templates/content', 'banner' ); ?>
		</div>
	</div>
	<!-- PAGE CONTENT -->
	<div id="primary" class="content-area content-outer">
		<div class="content-inner">
			
			<!-- ABOUT -->
			<div id="meet"></div>
			<div class="aboutme">
				<div class="aboutme-inner">
					Christi Leeson is an expert with 13 years experience in <strong>brand strategy, website design, and graphic design,</strong> 
					contributing these skills across print, web, and email mediums to enhance your strategic plan and vision. She is 
					team player who self-manages effectively and enjoys leading and collaborating on challenging projects that demand a 
					strong balance of excellence and time sensitivity.  She values evolving and streamlining.  She enjoys off-the-beaten-path 
					travel and pursuing the ultimate green home. <a href="<?php 
					echo esc_url( get_template_directory_uri() ); ?>/images/Christina_Leeson_resume.pdf" target="_blank">Resume&nbsp;&raquo;</a>
				</div>
			</div>
			
			<!-- SKILLS -->
			<div class="skills">
				<div class="skill graphic-design">
					<div id="graphic-design"></div>
					<h3>Graphic Design</h3>
					<img class="skill-icons" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icons-adobe-square.png" 
						alt="Adobe Suite - InDesign, Illustrator, Photoshop" />
					<p><span style="white-space: nowrap;">Graphic design for print and web mediums,</span> using Adobe Suite, 
						created for small businesses, non profits, and major banking and pharmaceutical corporations.  <a 
						href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Christina_Leeson_resume.pdf" 
						target="_blank">Resume&nbsp;&raquo;</a></p>
					<div id="slider-gd" class="slider-infinite">
						<div class="slide-mask">
							<button class="slide-left"></button>
							<div class="slider-body">
								<?php get_template_part( 'templates/content', 'portfolio' ); ?>
							</div>
							<button class="slide-right"></button>
						</div>
					</div>
				</div>
				<div class="skill web-development">
					<div id="website-development"></div>
					<h3>Web Development</h3>
					<img class="skill-icons" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icons-webdev-square.png" 
						alt="Developer tools - Sass, Bootstrap, Git, Wordpress" />
					<p><span style="white-space: nowrap;">Tools include Website Architecture &bull;</span> Ecommerce flows 
						&bull; Responsive Design &bull; Major browsers &amp; devices &bull; HTML5 &bull; SASS &bull; BootStrap 
						&bull; jQuery &bull; JavaScript &bull; WordPress &bull; CMSMS &bull; SEO &amp; Google Analytics &bull; 
						W3 Standards &bull; Constant Contact &bull; DataMail &bull; <a href="<?php 
						echo esc_url( get_template_directory_uri() ); ?>/images/Christina_Leeson_resume.pdf" 
						target="_blank">Resume&nbsp;&raquo;</a></p>
					<div id="slider-wd" class="slider-infinite">
						<div class="slide-mask">
							<button class="slide-left"></button>
							<div class="slider-body">
								<?php get_template_part( 'templates/content', 'webdevelopment' ); ?>
							</div>
							<button class="slide-right"></button>
						</div>
					</div>
				</div>
				<div class="skill illustration">
					<div id="photography"></div>
					<h3>Photography</h3>
					<p><span style="white-space: nowrap;">Ability to take set up photoshoots, position subjects</span> including 
						adults and children, and take strong, clear photography. <a href="<?php echo esc_url( get_template_directory_uri() ); 
						?>/images/Christina_Leeson_resume.pdf" target="_blank">Resume&nbsp;&raquo;</a></p>
					<div id="slider-ph" class="slider-infinite">
						<div class="slide-mask">
							<button class="slide-left"></button>
							<div class="slider-body">
								<?php get_template_part( 'templates/content', 'photography' ); ?>
							</div>
							<button class="slide-right"></button>
						</div>
					</div>
				</div>
				<div class="skill illustration">
					<div id="illustration"></div>
					<h3>Illustration</h3>
					<p><span style="white-space: nowrap;">lllustrations created in Adobe Suite</span> and/or using paper, pen, marker, 
						and paint mediums.  <a href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Christina_Leeson_resume.pdf" 
						target="_blank">Resume&nbsp;&raquo;</a></p>
					<div id="slider-il" class="slider-infinite">
						<div class="slide-mask">
							<button class="slide-left"></button>
							<div class="slider-body">
								<?php get_template_part( 'templates/content', 'illustration' ); ?>
							</div>
							<button class="slide-right"></button>
						</div>
					</div>
				</div>
			</div>
			
			<!--  FORM  -->
			<div id="contact"></div>
			<main id="main" class="site-main" role="main">
			<?php
				// Start the loop.
				while ( have_posts() ) : the_post();
					// Include the page content template.
					get_template_part( 'templates/content', 'page' );
				// End the loop.
				endwhile;
			?>
			</main><!-- .site-main -->
			
			<!--  FOOTER  -->
			<footer>
				<p>Copyright &copy; 2015, Christi&nbsp;Leeson, All&nbsp;rights&nbsp;reserved<br />Some images are property of clients</p>
			</footer>
			
		</div><!-- .content-inner -->
	</div><!-- .content-area content-outer -->
</div>

<?php
get_footer();
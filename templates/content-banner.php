<?php 
/**
 * Content template for Banner posts
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @package christileeson.com Theme
 * @author Christi Leeson 
 */

$banners = new WP_Query( array(
	'post_type'		=> CL_Post_Types::$banner_key,
	'tax_query'		=> array( array(
		'taxonomy'	=> CL_Post_Types::$banner_group_key,
		'field'		=> 'slug',
		'terms'		=> 'homepage'
	))
));

if ( $banners->have_posts() ) :
	while( $banners->have_posts() ) :
		$banners->the_post();
?>
	<article class="banner">
		<?php the_post_thumbnail(); ?>
		<div class="message">
			<h2 class="title"><?php the_title(); ?></h2>
			<div class="entry"><?php the_content(); ?></div>
		</div>
	</article>
<?php 
	endwhile;
endif;
wp_reset_query();
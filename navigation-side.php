<?php
/**
 * Side menu navigation styles
 *
 * @since 1.0.0
 * @version 1.0.0
 * @package christileeson.com Theme
 * @author Christi Leeson
 */

wp_nav_menu( array(
		'theme_location'	=> 'header-nav',
		'container'			=> 'nav'
));
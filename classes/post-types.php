<?php 
/**
 * Register post types used by the site
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @package christileeson.com Theme
 * @author Christi Leeson 
 */

class CL_Post_Types {
	public static $banner_key			= 'cl-banners';
	public static $banner_group_key		= 'cl-banners-group';
	public static $portfolio_key		= 'cl-portfolio';
	public static $portfolio_group_key	= 'cl-portfolio-group';
		
	public static function init() {
		add_theme_support( 'post-thumbnails' );
		self::register_banners();
		self::register_portfolio();
	}
	
	/**
	 * Register Banner post type and taxonomy
	 * 
	 * @return boolean false if it already exists
	 */
	private static function register_banners() {
		global $cl_banners;
		
		if ( !empty( $cl_banners ) ) {
			return false;
		}
		
		$cl_banners = new CPT_Maker(
				array(
					'key'		=> self::$banner_key,
					'singular'	=> 'Banner'
				),
				'-',
				array(
					'menu_position'	=> 10
				)
		);
		$cl_banners->add_taxonomy(
				array(
					'key'		=> self::$banner_group_key,
					'singular'	=> 'Banner Group',
				),
				'-'
		);
		$cl_banners->register();
	}
	
	/**
	 * Register Banner post type and taxonomy
	 *
	 * @return boolean false if it already exists
	 */
	private static function register_portfolio() {
		global $cl_portfolio;
	
		if ( !empty( $cl_portfolio ) ) {
			return false;
		}
	
		$cl_portfolio = new CPT_Maker(
				array(
					'key'		=> self::$portfolio_key,
					'singular'	=> 'Portfolio Piece'
				),
				'-',
				array(
					'menu_position'	=> 11,
					'supports'		=> array ( 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes' )
				)
		);
		$cl_portfolio->add_taxonomy(
				array(
						'key'		=> self::$portfolio_group_key,
						'singular'	=> 'Portfolio Type',
				),
				'-'
		);
		$cl_portfolio->register();
	}
}
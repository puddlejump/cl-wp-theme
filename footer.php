<?php
/**
 * Theme Footer
 * Called via get_footer()
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @package christileeson.com Theme
 * @author Christi Leeson 
 */

get_sidebar(); 
?>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>

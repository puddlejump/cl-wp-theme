<?php
/**
 * The template used for displaying page content
 *
 * @since 1.0.0
 * @version 1.0.0
 * @package christileeson.com Theme
 * @author Christi Leeson 
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php 
			// post content from wordpress cms
			the_content(); 
		    // shows links to other pages
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">Pages: </span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">Page </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php edit_post_link( 'Edit', '<footer class="entry-footer"><span class="edit-link">', '</span></footer><!-- .entry-footer -->' ); ?>

</article><!-- #post-## -->
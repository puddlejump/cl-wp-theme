<?php 
/**
 * Content template for Illustratoin posts
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @package christileeson.com Theme
 * @author Christi Leeson 
 */

$portfolio = new WP_Query( array(
	'posts_per_page' => 20,
	'post_type'		=> CL_Post_Types::$portfolio_key,
	'tax_query'		=> array( array(
		'taxonomy'	=> CL_Post_Types::$portfolio_group_key,
		'field'		=> 'slug',
		'terms'		=> 'illustration'
	))
));

if ( $portfolio->have_posts() ) :
	while( $portfolio->have_posts() ) :
		$portfolio->the_post();
?>
	<article class="portfolio slide">
		<a class="group-illustration" href="<?php echo esc_url( wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ) ); ?>" title="<?php 
			the_title(); ?>"><?php the_post_thumbnail( 'medium' ); ?></a>
	</article>
<?php 
	endwhile;
endif;
wp_reset_query();
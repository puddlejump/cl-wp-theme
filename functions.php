<?php
/**
 * Functions for theme
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @package christileeson.com Theme
 * @author Christi Leeson 
 */

require_once( 'classes/post-types.php' );

global $cl_banners, $cl_portfolio;

function cl_add_scripts() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css' );
	wp_enqueue_style( 'christileeson', get_template_directory_uri() . '/css/main.css' );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( 'christileeson-js', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( 'colorbox-js', get_template_directory_uri() . '/js/jquery.colorbox-min.js', array( 'jquery' ), '1.6.1', true );
}
add_action( 'wp_enqueue_scripts', 'cl_add_scripts' );

function cl_menus() {
	register_nav_menus( array( 
  		'header-nav' => 'Primary Navigation Menu' 
	));
}
add_action( 'after_setup_theme', 'cl_menus' );

// Register custom post types
CL_Post_Types::init();
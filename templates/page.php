<?php 
/**
 * Template Name: page
 * Default display index page
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @package christileeson.com Theme
 * @author Christi Leeson 
 */
get_header();
?>

<div class="wrapper">


	<!-- HEADER -->
	<header class="top-header">
		<div class="site-title">
			
			<h1 class="banner-text name"><a href="<?php echo esc_url( home_url() ); ?>"><img src="<?php 
				echo get_template_directory_uri(); ?>/images/logo_white_bg.png" alt="Christi Leeson"></a></h1>
			<h2 class="banner-text skill-title">rediscover your&nbsp;brand</h2>
		</div>
	</header>
	<!--div class="banner-text-container visible-xs-block">
		<div class="banner-text name-intro">The online portfolio of</div>
		<div class="banner-text name">Christi Leeson</div>
		<div class="banner-text skill-title">Visual design and web development</div>
		<a class="btn btn-default" href="contact" role="button">Contact</a>
	</div-->
	
	<div class="nav">
		<a class="btn btn-default" href="graphic-design" role="button">Graphic Design</a>
		<a class="btn btn-default" href="web-development" role="button">Web Development</a>
		<a class="btn btn-default" href="illustration" role="button">Illustration</a>
		<a class="btn btn-default" href="meet-christi" role="button">Contact</a>
	</div>
	
	
	
	<!-- PAGE CONTENT -->
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'templates/content', 'page' );


		// End the loop.
		endwhile;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->
	
	

	<!-- SKILLS -->
	<div class="skills">
		<div class="skill graphic-design">
			<a href="graphic-design">
				<p>Graphic Design</p>
				<img src="<?php echo get_template_directory_uri(); ?>/images/icons-adobe.png" alt="Adobe Suite - InDesign, Illustrator, Photoshop" class="img-responsive center-block" />
			</a>
			
		</div>
		<div class="skill web-development">
			<a href="web-development">
				<p>Web Development</p>
				<img src="<?php echo get_template_directory_uri(); ?>/images/icons-webdev.png" alt="Developer tools - Sass, Bootstrap, Git, Wordpress" class="img-responsive center-block" />
			</a>
		</div>
		<div class="skill portrait">
			<a href="meet-christi">
				<p>Meet and Contact</p>
				<img src="<?php echo get_template_directory_uri(); ?>/images/portrait-circle.png" alt="Meet and contact Christi Leeson" class="img-responsive center-block" />
			</a>
		</div>
	</div>
	
	<!--  FOOTER  -->
	<div class="footer">
		<p>Copyright &copy; 2015, Christi&nbsp;Leeson, All&nbsp;rights&nbsp;reserved<br />Gallery images are property of clients</p>
	</div>
	
	
	
</div>

<?php
get_footer();
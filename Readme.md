# Bootstrap Theme Installation

## Pre-requisite Development Software

* Ruby
* NodeJS
* Compass

## Initialize on Other Developers/Machines

* Clone the respository into a WordPress site theme directory
* Run "npm install", to reinstall all modules according to their listed version numbers
* (Optional if Bower isn't installed) Install Bower: "npm install -g bower"
* Run "bower install", to reinstall all modules according to their listed version numbers

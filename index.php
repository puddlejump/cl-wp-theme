<?php 
/**
 * Default display index page
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @package christileeson.com Theme
 * @author Christi Leeson 
 */
get_header();
get_template_part( 'post', 'standard' ); 
get_footer();
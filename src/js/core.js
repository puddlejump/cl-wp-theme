/**
 * Core JS File
 */

var console = console || { log: function( e ) { return; } };
var cl = cl || {};



cl.theme = function( $ ) {
	function register_colorboxes() {
		$( '.group-graphic-design' ).colorbox( { rel: 'cbox-gd', slideshow: false, maxWidth:'95%', maxHeight:'95%' } );
		$( '.group-illustration' ).colorbox( { rel: 'cbox-il', slideshow: false, maxWidth:'95%', maxHeight:'95%' } );
		$( '.group-photography' ).colorbox( { rel: 'cbox-ph', slideshow: false, maxWidth:'95%', maxHeight:'95%' } );
		$( '.group-website-development' ).colorbox( { rel: 'cbox-wd', slideshow: false, maxWidth:'95%', maxHeight:'95%' } );
	}
	function register_sliders() {
		$( '#slider-gd' ).infiniteSlider( { overflow: false } );
		$( '#slider-il' ).infiniteSlider( { overflow: false } );
		$( '#slider-ph' ).infiniteSlider( { overflow: false } );
		$( '#slider-wd' ).infiniteSlider( { overflow: false } );
	}
	function sidemenu_links() {
		$( '#menu-side .menu-item a' ).on( 'click', function() {
			$( '#menu-side' ).collapse( 'hide' );
		});
	}
	return {
		init: function() {
			$( document ).ready( function() {
				register_colorboxes();
				sidemenu_links();
			});
			register_sliders();
		}
	}
}( jQuery ); 

cl.theme.init();